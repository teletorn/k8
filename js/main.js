
function onMessage(evt){
	try{

	}catch(Ex){
		console.log(Ex);
	}
}

var gender_selection, age, length, weight, hair_color, eye_color, education, smoking, diabet, alcohol, sport, ps, bmi_weight, bmi_height, maleData, femaleData, count, lang, bmi_value, svg_arrow_deg;
var percent;
/* SVG NOOLE ARVUD */

var low1 	= 0;
var high1 	= 100;

var low2 	= -16.5;
var high2	= 163.5;

jQuery(document).ready(function(){
	smoking = false; diabet = false; sport = false; ps = false;

	lang = 'et';
	// -- Load data
	loadResults();

	// -- Get EST translations
	getTranslations('et');

	// -- Languages click function
	jQuery('.languages div').click(function(){
		lang = jQuery(this).attr('data-language');
		console.log(lang);
		if(lang){
			getTranslations(lang);
		}
	});

	// -- Row button click
	jQuery('.row_nupud').click(function(){
		setValue(jQuery(this).attr('data-appearance'), jQuery(this).attr('data-value'));
		controllValues();
	});

	// -- Weight/height button click

	jQuery('.bmi_button').click(function(){
		jQuery('.bmi_button').removeClass("focus");
		jQuery(this).addClass("focus");
	});


	// -- BMI calculator click
	jQuery('.calc_button').click(function(){
		if (jQuery('.bmi_button.focus > .bmi_p_value').text().length < 4){
			jQuery('.bmi_button.focus > .bmi_p_value').text(jQuery('.bmi_button.focus > .bmi_p_value').text() + jQuery(this).text());
		}
		else {
			console.log("Liiga palju :)");
		}

		bmi_height = jQuery('.bmi_button[data-value="calc_cm"] > .bmi_p_value').text();
		bmi_weight = jQuery('.bmi_button[data-value="calc_kg"] > .bmi_p_value').text();

		if (bmi_height != "" && bmi_weight != ""){
			calculateBMI(bmi_height, bmi_weight);
		}

	})

	jQuery('.calc_button_erase').click(function(){
		var delete_text = jQuery('.bmi_button.focus > .bmi_p_value').text();
		jQuery('.bmi_button.focus > .bmi_p_value').text(delete_text.slice(0, -1));

		bmi_height = jQuery('.bmi_button[data-value="calc_cm"] > .bmi_p_value').text();
		bmi_weight = jQuery('.bmi_button[data-value="calc_kg"] > .bmi_p_value').text();

		if (bmi_height != "" && bmi_weight != ""){
			calculateBMI(bmi_height, bmi_weight);
		}
		else {
			jQuery('.bmi_value > .bmi_p_value').text("?");
			$('.view[data-view="5"] button[data-button="transparent"]').removeClass('visible');
			$('.bmi_weight').addClass('hidden');
		}
	})

	function calculateBMI(calc_height, calc_weight){

		bmi_value = calc_weight / Math.pow(calc_height / 100, 2);
		//Calculator limits
		if (bmi_value > 40)
		{
			bmi_value=40;
		}
		else if (bmi_value < 16)
		{
			bmi_value=16;
		}
		jQuery('.bmi_value > .bmi_p_value').text(bmi_value.toString().substr(0,4));
		$('.view[data-view="5"] button[data-button="transparent"]').addClass('visible');


		if (bmi_value <= 18.5){
			$('.bmi_weight').attr('data-translation', 'below_weight');
			getTranslations(lang);
		}
		else if(bmi_value >= 35.1){
			$('.bmi_weight').attr('data-translation', 'really_overly_weight');
			getTranslations(lang);
		}
		else if(bmi_value >= 30.1){
			$('.bmi_weight').attr('data-translation', 'overly_weight');
			getTranslations(lang);
		}
		else if(bmi_value >= 25.1){
			$('.bmi_weight').attr('data-translation', 'over_weight');
			getTranslations(lang);
		}
		else{
			$('.bmi_weight').attr('data-translation', 'normal_weight');
			getTranslations(lang);
		}

		$('.bmi_weight').removeClass('hidden');
	}

	/* Risk buttons functionality :) */
	jQuery('.risk_button_answer').click(function(){
		if (jQuery(this).hasClass('risk_button_active') == true){
			jQuery(this).removeClass('risk_button_active');
			jQuery(this).children('.risk_button_circle').css("transform", "translateX(5px)");
			jQuery(this).children('.risk_button_yes').addClass('risk_text');
			jQuery(this).children('.risk_button_no').removeClass('risk_text');
		}
		else{
			jQuery(this).addClass("risk_button_active");
			jQuery(this).children('.risk_button_circle').css("transform", "translateX(95px)");
			jQuery(this).children('.risk_button_no').addClass('risk_text');
			jQuery(this).children('.risk_button_yes').removeClass('risk_text');
		}
	});

	jQuery('.risk_button').click(function(){
		if(jQuery(this).attr('data-value') == 'smoking') {
			smoking = (smoking) ? false : true;
		}
		if(jQuery(this).attr('data-value') == 'lack_of_movement') {
			sport = (sport) ? false : true;
		}
		if(jQuery(this).attr('data-value') == 'diabetes') {
			diabet = (diabet) ? false : true;
		}
		if(jQuery(this).attr('data-value') == 'hypertension') {
			ps = (ps) ? false : true;
		}
		if(gender_selection === 'male'){
			processData(maleData);
		} else {
			processData(femaleData);
		}
	});

});

function controllValues(){
	//console.log(length, weight, hair_color, eye_color);
	if(education != null){
		$('.view[data-view="4"] button[data-button="transparent"]').addClass('visible');
	}

}

function setValue(appearance, value){
	$('.row_nupud[data-appearance="' + appearance + '"]').removeClass('selected_appearance');
	$('.row_nupud[data-appearance="' + appearance + '"][data-value="' + value + '"]').addClass('selected_appearance');

	if(appearance == 'length'){
		length = value;
	}
	if(appearance == 'weight'){
		weight = value;
	}
	if(appearance == 'hair_color'){
		hair_color = value;
	}
	if(appearance == 'eye_color'){
		eye_color = value;
	}
	if(appearance == 'education'){
		education = value;
	}
	if(appearance == 'smoking'){
		smoking = value;
	}
	if(appearance == 'alcohol'){
		alcohol = value;
	}
	if(appearance == 'sport'){
		sport = value;
	}
}

function loadResults(){
	$.ajax({
		type: "GET",
		url: "data/Meeste_10a_tn_haigestumiseks_teletorn_bmi_uus.csv",
		dataType: "text",
		success: function(res) {maleData = res}
	});
	$.ajax({
		type: "GET",
		url: "data/Naiste_10a_tn_haigestumiseks_teletorn_bmi_uus.csv",
		dataType: "text",
		success: function(res) {femaleData = res}
	});
}

function processData(allText, time=1) {
	count = null;
	var allTextLines = allText.split(/\r\n|\n/);
	for (var i=1; i<allTextLines.length; i++) {
		var data = allTextLines[i].split(',');
		if(time == 0){
			smoking = false;
			sport = false;
			diabet = false;
			ps = false;
		}
		if(	(data[0] == age.substr(0,2) || data[0] == age.substr(4,2))
				&& data[1] == smoking
				&& data[2] == sport
				&& data[3] == convertedField(education, 'education')
				&& data[4] == diabet
				&& data[5] == ps
				&& data[6] == Math.round(bmi_value)
		){
			console.log(data);
			percent = data[7];
			setTimeout(function(){
				showPercent();
			},200);
		}
	}
}

function convertedField(value, type){
	if(type === 'education'){
		var returnValue;
		switch(value){
			case 'pohiharidus':
				returnValue = 2;
				break;
			case 'keskharidus':
				returnValue = 1;
				break;
			case 'korgharidus':
				returnValue = 0;
				break;
			default:
				returnValue = 1;
				break;
		}
		return returnValue;
	}

}

function selectGender(gender){
	$('.choose_gender_images img').addClass('not_active');
	$('.choose_gender_images img[data-gender="' + gender + '"]').removeClass('not_active');
	gender_selection = gender;
	$('.view[data-view="2"] button[data-questionbtn="1"]').addClass('visible');
}

function goToView(viewNumber){
	$('.dragdealer').addClass('hidden');
	$(".view").removeClass("show");
	$(".view:nth-of-type("+viewNumber+")").addClass("show");
	if(viewNumber == 1){
		smoking = false; diabet = false; sport = false; ps = false;
		$('.risk_value h2').html('0%');
		resetAllSamples();
	}
	if(viewNumber == 3){
		$('.dragdealer').removeClass('hidden');
	}
	if(viewNumber == 6){
		if(gender_selection === 'male'){
			processData(maleData, 0);
		} else {
			processData(femaleData, 0);
		}
	}
}

function showPercent(){
	var newPercent = Math.round(percent * 100) / 100;
	$('.risk_value h2').html(newPercent + '%');
	var arrowDeg = ((180 * newPercent) / 100) - 16.5;
	$('.arrow').css('transform', 'rotate(' + arrowDeg + 'deg)');
}

// -- TRANSLATIONS
function getTranslations(lang) {
	jQuery('.languages div').removeClass('active');
	jQuery('.languages div[data-language="' + lang + '"]').addClass('active');
	var json = getLangJSON(lang);
	jQuery.each(json[0], function (key, data) {
		jQuery('[data-translation="' + key + '"]').html(data).val(data);
	});
}

function getLangJSON(lang){
	var json;
	if(lang == 'et'){
		json = translations.et;
	} else if(lang == 'en') {
		json = translations.en;
	} else if(lang == 'ru') {
		json = translations.ru;
	} else if(lang == 'fi') {
		json = translations.fi;
	}
	return json;
}

Number.prototype.map = function (in_min, in_max, out_min, out_max) {
	return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

function getAge(x){
	var number = parseInt(round(x, 1)).map(0,100, 20,75);
	var min = Math.floor(number/2)*2;
	var max = min + 1;
	age = min + '-' + max;
	$('.min_age').html(min);
	$('.max_age').html(max);
}

function round(value, precision) {
	var multiplier = Math.pow(10, precision || 0);
	return Math.round(value * multiplier) / multiplier;
}

// Custom Checking Function..
function inRangeInclusive(start, end, value) {
	if (value <= end && value >= start)
		return value; // return given value
	return undefined;
}


/* 	SVG arrow calculation

	Value is the % number from the csv file.	*/

function BMI_arrow_calc(value){
    svg_arrow_deg =	low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	jQuery('.arrow').css("transform", "rotate(" + svg_arrow_deg + "deg)")
	return svg_arrow_deg;
}